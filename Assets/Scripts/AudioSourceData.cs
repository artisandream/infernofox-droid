using UnityEngine;

[CreateAssetMenu]
public class AudioSourceData : ScriptableObject
{
    public AudioSource audioSourceObj;
    
    public void StopMusic()
    {
        //  if (instance is { }) instance.GetComponent<AudioSource>().Stop();
        // musicToggle = "Stop";
        audioSourceObj.Stop();
    }

    public void StartMusic()
    {
        //  if (userSetMusicTo != "on") return;
        // // if (instance is { }) instance.GetComponent<AudioSource>().Play();
        //  musicToggle = "Start";
        audioSourceObj.Play();
    }
}
