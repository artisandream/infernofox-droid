using UnityEngine;
using System.Collections;

public class JumpAndFuelScript : MonoBehaviour
{
	public GameAction jumpPlayer, addMoreFuel, endGame;
	public IntData score;

	private Vector2 v2;
	public float fuelUsage = .03f, fuelLevel = 1, 
		maxFuelLevel = 1, reFuelAmount = 0.33f, reFuelAmountMin = 0.1f, decreaseReFuel = 0.00001f, 
		fireEmissionRate = 200, smokeEmissionRate = 10;
	public ParticleSystem rocketFire;
	public ParticleSystem rocketSmoke;
	
	public float[] fuelLevels;
	private bool canJump = true;
	

	private void Start () {
		v2.y = 1;
		endGame.raiseNoArgs += EndThisGame;
		addMoreFuel.raiseNoArgs += Refuel;
	}

	private void OnDestroy () {
		endGame.raiseNoArgs -= EndThisGame;
		addMoreFuel.raiseNoArgs -= Refuel;
	}

	private void EndThisGame () {
		rocketFire.Stop();
		rocketSmoke.Stop();
		canJump = false;
		StartCoroutine(TurnOffRocket());
	}

	private void Refuel () {
		if(fuelLevel >= 0) {
			fuelLevel += reFuelAmount;
		}
		if(fuelLevel >= maxFuelLevel) {
			fuelLevel = maxFuelLevel;
		}
	}

	private void JumpCharacter ()
	{
		score.UpdateValue(1);
		if (!canJump) return;
		if(jumpPlayer != null)
			jumpPlayer.raiseNoArgs();

		if (fuelLevel > 0) {
			fuelLevel -= fuelUsage;
		}

		RocketFlames();

		if(reFuelAmount >= reFuelAmountMin) {
			reFuelAmount -= decreaseReFuel;
		}
		if(fuelLevel <= 0)
			canJump = false;
	}

	private void OnMouseDown () {
		JumpCharacter ();
	}

	private void RocketFlames (){
		var rocketFireEmission = rocketFire.emission;
		if (fuelLevel > fuelLevels[0]){
			rocketSmoke.Emit(0);
			rocketFire.Emit((int) ((fuelLevel)*fireEmissionRate));
		}
		if (fuelLevel > fuelLevels[2] && fuelLevel <= fuelLevels[1])
		{
			rocketFireEmission.rateOverTime = (fuelLevel*0.25f)*fireEmissionRate;
		}

		if (!(fuelLevel < fuelLevels[2])) return;
		var rocketSmokeEmission = rocketSmoke.emission;
		rocketSmokeEmission.rateOverTime = smokeEmissionRate;
		rocketFireEmission.rateOverTime = 0;
	}

	private IEnumerator TurnOffRocket () {
		yield return new WaitForSeconds(1);
		rocketFire.Stop();
		rocketSmoke.Stop();
	}
}

