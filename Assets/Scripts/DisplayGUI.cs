﻿using UnityEngine;
using System.Collections;

public class DisplayGUI : MonoBehaviour {

	private Animator guiAnim;
	public ParticleSystem endExplosion;
	public float reloadTime = 3;
	public GameAction endGame;

	private void OnEnable () {
		guiAnim = transform.GetComponent<Animator>();
		endGame.raiseNoArgs += EndThisGame;
	}

	private void OnDestroy () {
		endGame.raiseNoArgs -= EndThisGame;
	}

	private void EndThisGame ()
	{
		StartCoroutine(RunGUI());
	}

	private IEnumerator RunGUI() {
		endExplosion.GetComponent<Renderer>().sortingLayerName = "Foreground";
		endExplosion.Play();
		yield return new WaitForSeconds(reloadTime);
		const string anim = "Restart";
		guiAnim.SetBool (anim, true);
	}
}
