﻿using UnityEngine;
using System.Collections;

public class PlaySunEnd : MonoBehaviour {

	public Animator sunAnim;
	public GameAction endGame;
	
	private void Start () {
		endGame.raiseNoArgs += PlaySunAnim;
	}

	private void OnDestroy () {
		endGame.raiseNoArgs -= PlaySunAnim;
	}

	private void PlaySunAnim ()
	{
		const string anim = "End";
		sunAnim.SetBool (anim, true);
	}
}
