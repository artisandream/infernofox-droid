﻿using UnityEngine;
using System.Collections;
using System;

public class AddPoints : MonoBehaviour
{

	public IntData score;
	public static Action AddToScore;

	private void OnTriggerEnter2D (Collider2D _c)
	{
		score.value++;
		// if (AddToScore == null) return;
		// AddToScore();
		gameObject.SetActive(false);
	}
}
