﻿using UnityEngine;

public class AddMoreFuel : MonoBehaviour
{

	public IntData score;
	public int gasValue = 10;
	public GameAction addMoreFuelAction;

	private void OnTriggerEnter2D(Collider2D other)
	{
		score.UpdateValue(gasValue);
		if (addMoreFuelAction == null) return;
		addMoreFuelAction.raiseNoArgs();
		gameObject.SetActive(false);
	}
} 