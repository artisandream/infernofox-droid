﻿// Anthony Romrell ©2014

using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu]
public class StaticVars : ScriptableObject 
{
	public int counter = 0;
	public int clickTimes = 2;
	public int highScore;
	public List<ScoreClass> scoreClassList;
	//public LevelEnum.LevelChoice currentLevel;
}
