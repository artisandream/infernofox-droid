﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundRecordingSelector : SoundSelector {
	
	public AudioClipData userRecordedClip;
	
	protected override void OnClick ( ) {
		audioSourceObj.clip = userRecordedClip.clip;
		audioClipDataObj.clip = userRecordedClip.clip;
		audioSourceObj.Play ();
	}
}