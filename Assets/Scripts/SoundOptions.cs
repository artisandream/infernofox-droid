﻿using UnityEngine;
using System.Collections;
using System;

public class SoundOptions : MonoBehaviour {

	public string level;
	public static Action TurnOffGasCanSound;

	private void OnClick () {

		if(TurnOffGasCanSound != null)
			TurnOffGasCanSound();

		Application.LoadLevel(level);
	}
}
