﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowEndGameScreen : MonoBehaviour
{
	public GameAction endGame;
	private void Start () 
	{
		endGame.raiseNoArgs += TurnOn;
	}

	private void OnDestroy () {
		endGame.raiseNoArgs -= TurnOn;
	}

	private void TurnOn () 
	{
		this.gameObject.GetComponent<Animator>().SetBool("GameOver", true);
	}
}
