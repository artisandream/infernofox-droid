﻿using UnityEngine;

public class EnableRecycle : MonoBehaviour 
{
	public GameObject[] tileChildren;
	public GameAction addToListAction;
	
	private void RecycleTransform ()
	{
		addToListAction.raise(this);
	}

	public void EnableChildren()
	{
		foreach (var go in tileChildren)
		{
			go.SetActive(true);
		}
	}

	private void Start () {
		RecycleTransform ();
	}

	// private void OnTriggerEnter2D (Collider2D obj) {
	// 	RecycleTransform ();
	// }
}	