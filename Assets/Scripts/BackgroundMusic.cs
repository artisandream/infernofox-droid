﻿using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    //private BackgroundMusic instance = null;
    public string userSetMusicTo = "on";
    public string musicToggle = "Start";

    public AudioClipData musicClip;

    public AudioSourceData audioSourceDataObj;
    // public static BackgroundMusic Instance
    // {
    //     get { return instance; }
    // }

    public AudioSource audioSourceObj;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        audioSourceObj = GetComponent<AudioSource>();
        audioSourceDataObj.audioSourceObj = audioSourceObj;
        audioSourceDataObj.audioSourceObj.Play();
//        audioSourceObj.clip = musicClip.clip;
        
        //   GetComponent<AudioSource>().Stop();
        //   if (instance.GetComponent<AudioSource>().clip != GetComponent<AudioSource>().clip)
        //   {
        //        instance.GetComponent<AudioSource>().clip = GetComponent<AudioSource>().clip;
        //        instance.GetComponent<AudioSource>().volume = GetComponent<AudioSource>().volume;
        //        instance.GetComponent<AudioSource>().Play();
        //   }

        //  Destroy(this.gameObject);
        //   return;


        //  instance = this;
        //GetComponent<AudioSource>().Play();
    }

    public void StopMusic()
    {
        //  if (instance is { }) instance.GetComponent<AudioSource>().Stop();
       // musicToggle = "Stop";
        audioSourceObj.Stop();
    }

    public void StartMusic()
    {
        //  if (userSetMusicTo != "on") return;
        // // if (instance is { }) instance.GetComponent<AudioSource>().Play();
        //  musicToggle = "Start";
        audioSourceObj.Play();
    }
}