﻿using UnityEngine;

public class PlayerControl : MonoBehaviour {
	
	public GameAction jumpAndFuelScript;
	public Rigidbody2D playerRigid;
	public float myForce = 20;
	public float increaseMass = 0.0002f;
	private Vector2 v2;

	private void Start() {
		v2.y = 1;
		jumpAndFuelScript.raiseNoArgs += JumpPlayer;
	}

	private void OnDestroy () {
		jumpAndFuelScript.raiseNoArgs -= JumpPlayer;
	}

	private void JumpPlayer () {
		playerRigid.AddForce(v2 * myForce);
		playerRigid.mass += increaseMass;
	}
}
