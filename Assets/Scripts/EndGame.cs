using UnityEngine;
using System.Collections;
using System;

public class EndGame : MonoBehaviour
{
	public GameAction endGameAction;
	private void OnTriggerEnter2D (Collider2D c)
	{
		endGameAction.raiseNoArgs();
	}
}