﻿using UnityEngine;
using System.Collections;

public class SoundsBackgroundToggle : MonoBehaviour
{

	public AudioSource audioSourceObj;

	public void SetSource(AudioSource obj)
	{
		audioSourceObj = obj;
	}
	
	public void StopMusic()
	{
		//  if (instance is { }) instance.GetComponent<AudioSource>().Stop();
		// musicToggle = "Stop";
		audioSourceObj.Stop();
	}

	public void StartMusic()
	{
		//  if (userSetMusicTo != "on") return;
		// // if (instance is { }) instance.GetComponent<AudioSource>().Play();
		//  musicToggle = "Start";
		audioSourceObj.Play();
	}
}
