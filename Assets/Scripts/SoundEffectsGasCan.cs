﻿using UnityEngine;
using System.Collections;
using System;

public class SoundEffectsGasCan : MonoBehaviour {
	
	public AudioClip gasCanSound;
	public GameAction addMoreFuel, endGame;

	private void Start(){
		GetComponent<AudioSource>().clip = gasCanSound;
	}

	private void Awake(){
		DontDestroyOnLoad(this.gameObject);
	}

	private void OnEnable (){
		addMoreFuel.raiseNoArgs += PlayGasCanSound;
		endGame.raiseNoArgs += TurnOffGasCanSound;
	}

	private void PlayGasCanSound () {
		GetComponent<AudioSource>().Play ();
	}

	private void TurnOffGasCanSound () {
		addMoreFuel.raiseNoArgs -= PlayGasCanSound;
	}
}
