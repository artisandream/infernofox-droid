﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class MoveAndLevelGenerate : MonoBehaviour {
	
	public List<EnableRecycle> recycleList, reserveList;
	public GameAction addToListAction, endGameAction;
	public float speed = 2.1f, increasedSpeed = 0.05f,
		distance, nextDistance;
	public Vector2 currentVector;
	private new Rigidbody2D rigidbody2D;
	private bool canRun = true;
	private WaitForFixedUpdate wffu;
	
	public IEnumerator Start () {
		endGameAction.raiseNoArgs += StopLoop;
		addToListAction.raise += AddToEnableList;
		rigidbody2D = GetComponent<Rigidbody2D>();
		wffu = new WaitForFixedUpdate();
		
		while (canRun)
		{
			yield return wffu;
			transform.Translate(Vector3.right * (Time.deltaTime * speed));
			if ((transform.position.x > nextDistance))
			{
				NextGenerate();
			}
			
		}
	}

	private void StopLoop()
	{
		canRun = false;
	}

	private void AddToEnableList (object obj)
	{
		var newObj = obj as EnableRecycle;
		recycleList.Add(newObj);
	}

	
	private void NextGenerate ()
	{
		if (recycleList.Count <= 0) return;
		nextDistance = transform.position.x + distance;
		var num = Random.Range(0, recycleList.Count-1);
		var obj = recycleList[num];
		recycleList.RemoveAt(num);
		reserveList.Add(obj);
		if (recycleList.Count < 8)
			
		{
			recycleList.Add(reserveList[0]);
			reserveList.RemoveAt(0);
		}

		currentVector.x = nextDistance;
		currentVector.y = 0;
		obj.transform.position = currentVector;
		obj.EnableChildren();
		speed += increasedSpeed;
	}
}