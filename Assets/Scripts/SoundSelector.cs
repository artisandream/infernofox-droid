﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundSelector : MonoBehaviour {

	public AudioClip userSelectedSound;
	public AudioClipData audioClipDataObj;
	protected AudioSource audioSourceObj;

	private void Start()
	{
		audioSourceObj = GetComponent<AudioSource>();
		audioSourceObj.clip = userSelectedSound;
	}

	protected virtual void OnClick ( ) {
		audioClipDataObj.clip = userSelectedSound;
		audioSourceObj.Play();
	}
}