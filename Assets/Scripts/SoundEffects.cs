﻿using UnityEngine;

public class SoundEffects : MonoBehaviour
{
	public GameAction jumpAndFuelScript, endGame;
	public bool boomSoundStarted = false;
	public AudioClip[] boomSound;
	public AudioClip initialFoxSound;
	public AudioClipData myFoxSound;
	private AudioSource audioSourceObj;

	private void Awake()
	{
		if (myFoxSound.clip == null)
			myFoxSound.clip = initialFoxSound;
		audioSourceObj = GetComponent<AudioSource>();
	}

	private void OnEnable (){
		endGame.raiseNoArgs += PlayExplosionSound;
		jumpAndFuelScript.raiseNoArgs += PlayJumpSound;
	}

	private void OnDestroy (){
		endGame.raiseNoArgs -= PlayExplosionSound;
		jumpAndFuelScript.raiseNoArgs -= PlayJumpSound;
	}

	private void PlayExplosionSound ()
	{
		audioSourceObj.clip = boomSound[0];
		audioSourceObj.Play ();
		boomSoundStarted = true;
	}

	private void PlayJumpSound ()
	{
		if (boomSoundStarted) return;
		if(myFoxSound == null){
			myFoxSound.clip = initialFoxSound;
		}

		audioSourceObj.clip = myFoxSound.clip;
		audioSourceObj.Play ();
	}
}