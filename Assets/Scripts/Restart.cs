﻿using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

	public string restartLevel = "GamePlay";

	private void OnClick () {
		Application.LoadLevel(restartLevel);
	}
}
