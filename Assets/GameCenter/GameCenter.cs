using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;

[CreateAssetMenu]
public class GameCenter : ScriptableObject
{
    public IntData score, highScore;
    public DataStorage dataStorageObj;

    public void CheckScore()
    {
        dataStorageObj.GetData(highScore);
        
        if (score.value < highScore.value) return;
        highScore.value = score.value;
        dataStorageObj.SetData(highScore);
    }
}