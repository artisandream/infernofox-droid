using UnityEngine;

[CreateAssetMenu]
public class AudioData : ScriptableObject
{
    public AudioClip value;
}
