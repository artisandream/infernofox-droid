﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]

public class SingleMicrophoneCapture : MonoBehaviour 
{
	private bool micConnected = false;
	public UILabel recordLabel;

	public AudioClipData audioClipDataObj, recordedClipDataObj;

	private int minFreq, maxFreq;
	
	public AudioClip metronomeSound, mySavedFileSound;

	public UISlider audioSlider;
	public UIButton myRecordButton;

	public AudioData audioDataObj;
	public DataStorage dataStorageObj;
	private SavWav savWavObj;

	private AudioSource audioSourceObj;
	private BoxCollider boxCollider;

	private void Awake()
	{
		boxCollider = GetComponent<BoxCollider>();
	}

	private void Start()
	{
		audioSourceObj = GetComponent<AudioSource>();
		if (Microphone.devices.Length <= 0) return;
		micConnected = true;
		savWavObj = new SavWav();
		savWavObj.LoadFile("clip", mySavedFileSound);
	}

	private void OnClick ()
	{
		RecordSound ();
	}

	private void RecordSound()
	{
		if (!micConnected) return;
		if(!Microphone.IsRecording(null))
		{
			StartCoroutine(RecordAndSave());
		}
	}
	
	private IEnumerator RecordAndSave() {
		
		GetComponent<BoxCollider>().enabled = false;
		myRecordButton.defaultColor = Color.green;
		recordLabel.text = "Wait for It...";
		
		//BackgroundMusic.StopMusic(); 
		
		audioSourceObj.clip = metronomeSound;
		audioSourceObj.Play ();
		
		yield return new WaitForSeconds(2);
		recordLabel.text = "Go! Go! Go!";
		myRecordButton.defaultColor = Color.red;

		audioSlider.value = 0;
		mySavedFileSound = Microphone.Start(null, true, 1, 44100);
		
		yield return new WaitForSeconds(1);
		
		recordLabel.text = "Record";
		myRecordButton.defaultColor = Color.white;
		
		//End Recording
		Microphone.End(null); //Stop the audio recording

		//Playback Recording
		audioSourceObj.clip = mySavedFileSound;
		audioSourceObj.Play(); 
		
		audioClipDataObj.clip = mySavedFileSound;
		recordedClipDataObj.clip = mySavedFileSound;
		//BackgroundMusic.StartMusic();

		savWavObj.Save("clip", mySavedFileSound);
		
		audioDataObj.value = mySavedFileSound;
		dataStorageObj.SetData(audioDataObj);
		 
		
		boxCollider.enabled = true;
	}
	
}